const express = require('express')

const app = express()

app.use(express.static(__dirname + '/islamicos-web-app/build'))

const port = process.env.PORT || 3000

app.listen(port, () => console.log(`server running on port ${port}`))
